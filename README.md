This is a TensorFlow example implementation for retina classification. The network is a standard residual network model with 50 layers which is defined in resnet_models.py and resnet_utils.py.



## 1 Generating training binaries
The folder ./lists/ contains lists which are pointing towards original kaggle data.
```bash
python3 convert_numpy_to_balanced_tfrecord.py
```
This call converts all images from the list into several .bin files which are optimized for serialized reading from disc. Those .bin files are used by the training routine.

## 2 Training the model
to train the model call:
```bash
python3 drd_train_v2.py
```
the model parameters get frequently stored in ./output. A TF model consists of 4 files:<br>
model.ckpt-10.meta <br>
model.ckpt-10.index<br>
model.ckpt-10.data-00000-of-00001<br>
checkpoint<br>


## 3 Predicting an image (current version)

This version of the code directly loads the standard graph saving format of tensorflow (4 file version)
In order to perform example image classifcation the network, execute
```bash
python3 drd_eval_v2.py
```

## 4 Freezing the TF graph
The module tensorflow_graph_freezer.py converts the model from step 2 into the single 'model.pb' protobuf file for deployment. This conversion still suffers produces an error at the moment.
```bash
python3 tensorflow_graph_freezer.py --model_dir ./output/resnet_2BLock_drd_train/ --output_node_names='resnet_v2_50/logits'
```


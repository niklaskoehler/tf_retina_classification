
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import os.path
import time
import unicodedata
from matplotlib import pyplot as plt
from PIL import Image

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import drd
import sys

from resnet_models import resnet_v2_50

#write no pyc files
sys.dont_write_bytecode = True

parser = drd.parser

parser.add_argument('--train_dir', type=str, default='./output/resnet_2BLock_drd_train',
                    help='Directory where to write event logs and checkpoint.')

parser.add_argument('--pre_trained_dir', type=str, default='./output/pre_weights',
                    help='Directory where to write event logs and checkpoint.')

parser.add_argument('--max_steps', type=int, default=2000000,
                    help='Number of batches to run.')

parser.add_argument('--log_device_placement', type=bool, default=False,
                    help='Whether to log device placement.')

parser.add_argument('--log_frequency', type=int, default=10,
                    help='How often to log results to the console.')

parser.add_argument('--n_residual_blocks', type=int, default=5,
                    help='Number of residual blocks in network')

width = 232
height = width

def predict_single_image():
    """Train CIFAR-10 for a number of steps."""
    with tf.Graph().as_default():
        global_step = tf.Variable(0, trainable=False, name= 'global_step')

        np_image = np.array(Image.open('test.tiff').resize((width, height), Image.ANTIALIAS))
        np_image = np_image.astype(np.float) / 256.
        np_image = np.expand_dims(np_image, axis=0)

        # Build a Graph that computes the logits predictions from the
        # inference model.
        #logits1= drd.inference(images, FLAGS.n_residual_blocks)
        #logits = drd.oxford_net_C(images)
        images = tf.placeholder(dtype = tf.float32, shape=[None, width,height,3])
        logits, net = resnet_v2_50(images, num_classes=5, global_pool=True)
        logits = tf.squeeze(logits)
        softmax = tf.nn.softmax(logits)
        print(logits)
        # calculate predictions
        predictions = tf.cast(tf.argmax(softmax, axis=1), tf.int32)

        # Create a saver.
        saver = tf.train.Saver(tf.global_variables())

        # Build an initialization operation to run below.
        init = tf.global_variables_initializer()
        # Start running operations on the Graph.
        sess = tf.Session(config=tf.ConfigProto(
            log_device_placement=FLAGS.log_device_placement))
        # sess.run(init)

        print("Trying to restore last checkpoint ...")
        save_dir = FLAGS.train_dir
        # Use TensorFlow to find the latest checkpoint - if any.
        last_chk_path = tf.train.latest_checkpoint(checkpoint_dir=save_dir)
        # Try and load the data in the checkpoint.
        saver.restore(sess, save_path=last_chk_path)

        # If we get to this point, the checkpoint was successfully loaded.
        print("Restored checkpoint from:", last_chk_path)
        # get the step integer from restored path to start step from there

        out = sess.run([softmax], feed_dict = {images : np_image})
        print(out)



def main(argv=None):  # pylint: disable=unused-argument
    predict_single_image()


if __name__ == '__main__':
    FLAGS = parser.parse_args()
    tf.app.run()
